# BTC Price API

## Overview

BTC Price API is a simple web application that fetches the current Bitcoin price from CoinDesk and provides it through an HTTP API. The application is built using Go and utilizes OpenTelemetry for tracing. It is designed to be deployed on Kubernetes using Helm.

The idea of this app its only to be used as a simple example and do not provide any real feature to be used. 

## Prerequisites

- [Go](https://golang.org/dl/) 1.22 or later
- [Docker](https://www.docker.com/products/docker-desktop)
- [Minikube](https://minikube.sigs.k8s.io/docs/start/)
- [Helm](https://helm.sh/docs/intro/install/)
- [yq](https://github.com/mikefarah/yq)

## Getting Started

### Clone the Repository

```sh
git clone <repository-url>
cd btc-price-api
```

### Building the Application

```sh
docker build -t btc-price-api:latest .
```

### Running the Application Locally

```sh
go run .
```

The application will start on port `8080`.

### API Endpoints

- `/btc-price` - Fetches the current Bitcoin price from CoinDesk.
- `/healthz` - Health check endpoint.

### Example Request

```sh
curl http://localhost:8080/btc-price
```

## OpenTelemetry Integration

The application is instrumented with OpenTelemetry to provide distributed tracing. Traces are exported to Honeycomb.

### Configuration

The tracing configuration is managed through environment variables defined in the `values.yaml` file.

- `otelProto`: Protocol to use for exporting traces (e.g., `http/protobuf`).
- `otelEndpoint`: Endpoint to send traces to (e.g., `https://api.honeycomb.io`).
- `otelHeaders`: Headers required for trace exporting (e.g., `x-honeycomb-team=xxxx`).

### Deployment

#### Using Minikube

1. Start Minikube:

    ```sh
    minikube start
    ```

2. Build the Docker image and deploy to Minikube:

    ```sh
    ./deploy.sh
    ```

#### Bumping the Application Version

To bump the application version, use the `--bump true` argument with the `deploy.sh` script:

```sh
./deploy.sh --bump true
```

## Helm Chart

The application can be deployed using a Helm chart. The chart values are configured in `values.yaml`.

### Installing the Chart

```sh
helm upgrade -i -f values.yaml btc-price-api chart
```

## License

This project is licensed under the MIT License. See the [LICENSE](LICENSE) file for details.

## Contributing

Contributions are welcome! Please open an issue or submit a pull request.

## Contact

For any inquiries, please contact cesar.sepulveda AT gmail.com.

