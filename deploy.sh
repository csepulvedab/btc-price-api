#!/bin/bash
# check is argument --bump true exist to bump the appVersion using yq:
if [ "$1" == "--bump" ] && [ "$2" == "true" ]; then
  yq e -i '
  .appVersion |=
  (split(".") | 
    .[0] + "." + 
    (.[1] | tonumber + 1 | tostring) + 
    ".0"
  )' chart/Chart.yaml
fi
docker build . -t btc-price-api:$(yq .appVersion chart/Chart.yaml)
helm upgrade -i  -f values.yaml btc-price-api chart 